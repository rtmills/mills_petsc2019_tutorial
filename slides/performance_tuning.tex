%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{FLOPS and Memory Bandwidth}

\begin{block}{Operations in PETSc tend to}
\begin{itemize}
\item Deal with large datasets (vectors, sparse matrices)
\item Perform few arithmetic operations per byte loaded/stored from main memory; \\
      this ratio, the {\it arithmetic intensity}, is usually below unity.
\end{itemize}
\end{block}

\begin{block}{Modern CPUs support arithmetic intensity around 10 at full utilization}
\begin{itemize}
\item Most operations in PETSc are limited by the rate at which data can be loaded/stored; \\
      they are {\it memory bandwidth limited}. (We know this from both models and measurements. 
      More on this later.)
\end{itemize}
\end{block}

\begin{block}{Maximizing use of available memory bandwidth is key!} 
\begin{itemize}
\item Process placement is critical on NUMA systems
\item Read/write contiguous blocks of memory
\item Avoid unordered reads whenever possible
\item Vectorization doesn't matter if poor memory bandwidth utilization means VPUs cannot be kept busy!
\end{itemize}
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Memory Bandwidth vs.\ Processes}

%\begin{block}{Consider the STREAM Triad benchmark}
\begin{itemize}
\item STREAM Triad computes $\mathbf{w} = \mathbf{y} + \alpha \mathbf{x}$ for large arrays (exceeding cache size)
\item Usually saturates quickly; 8-16 processes/threads sufficient for most modern server CPUs
\item Little speedup to be gained after this saturation point
\end{itemize}
%\end{block}

\begin{center}
\includegraphics[width=0.7\textwidth]{figures/stream}
\end{center}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Multiple sides here: Karl's strided access slides.

\input{slides/strided_access.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Check Memory Bandwidth Yourself}

\begin{itemize}
\item Set \$PETSC\_ARCH and then {\tt make streams} in \$PETSC\_DIR:
\end{itemize}

\begin{lstlisting}
np  speedup
1 1.0
2 1.58
3 2.19
4 2.42
5 2.63
6 2.69
...
21 3.82
22 3.49
23 3.79
24 3.71
Estimation of possible speedup of MPI programs based on Streams benchmark.
It appears you have 1 node(s)
\end{lstlisting}

\begin{itemize}
\item Expect max speedup of 4X on this machine when running typical PETSc app with multiple MPI ranks on the node
\item Most gains already obtained when running with 4--6 ranks.
\end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Non-Uniform Memory Access (NUMA) and Process Placement}

\begin{block}{Modern compute nodes are typically multi-socket:}
\end{block}

\begin{center}
\includegraphics[width=0.9\textwidth]{figures/numa.pdf}
\end{center}

\begin{block}{Non-uniform memory access (NUMA):}
\begin{itemize}
\item A process running on one socket has direct access to the memory channels of its CPU...
\item ...but requests for memory attached to a different socket must go through the interconnect
\item To maximize memory bandwidth, processes should be distributed evenly between the sockets
\end{itemize}
\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Non-Uniform Memory Access (NUMA) and Process Placement}

\begin{block}{Example: 2 sockets, 6 cores per socket, 2 hardware threads per core}
~\\
Processes all mapped to first socket:
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,escapechar=\#]
$ mpirun -n 6 --bind-to core --map-by core ./stream
process 0 binding: 100000000000100000000000
process 1 binding: 010000000000010000000000
process 2 binding: 001000000000001000000000
process 3 binding: 000100000000000100000000
process 4 binding: 000010000000000010000000
process 5 binding: 000001000000000001000000
Triad:        25510.7507   Rate (MB/s)
\end{lstlisting}

Processes spread evenly between sockets:
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,escapechar=\#]
$ mpirun -n 6 --bind-to core --map-by socket ./stream
process 0 binding: 100000000000100000000000
process 1 binding: 000000100000000000100000
process 2 binding: 010000000000010000000000
process 3 binding: 000000010000000000010000
process 4 binding: 001000000000001000000000
process 5 binding: 000000001000000000001000
Triad:        45403.1949   Rate (MB/s)
\end{lstlisting}

\end{block}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Cannot assume that mpirun defaults to sensible placement!}

\begin{minipage}{0.23\textwidth}
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,escapechar=\#]
$ make streams
np  speedup
1 1.0
2 1.58
3 2.19
4 2.42
5 2.63
6 2.69
7 2.31
8 2.42
9 2.37
10 2.65
11 2.3
12 2.53
13 2.43
14 2.63
15 2.74
16 2.7
17 3.28
18 3.66
19 3.95
20 3.07
21 3.82
22 3.49
23 3.79
24 3.71
\end{lstlisting}
\end{minipage} \hfill
\begin{minipage}{0.72\textwidth}
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize, escapechar=\#]
$ make streams MPI_BINDING="--bind-to core --map-by socket"
np  speedup
1 1.0
2 1.59
3 2.66
4 3.5
5 3.56
6 4.23
7 3.95
8 4.39
9 4.09
10 4.46
11 4.15
12 4.42
13 3.71
14 3.83
15 4.08
16 4.22
17 4.18
18 4.31
19 4.22
20 4.28
21 4.25
22 4.23
23 4.28
24 4.22
\end{lstlisting}
\end{minipage}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Additional Process Placement Considerations and Details}

\begin{itemize}
\item Primary consideration: distribute MPI processes evenly distributed among sockets, thus using all available memory channels.
\item Increasingly complex designs, however, mean that performance may also be sensitive to how 
processes are bound to the resources {\it within each socket}.
\item Preceding examples relatively insensitive: 
one L3 cache is shared by all cores within a NUMA domain, and each core has its own L2 and L1 caches. 
\item Processors that are less ``flat'', with more complex hierarchies, may be more sensitive.
%\item In many AMD Opterons or the ``Knights Landing'' Intel Xeon Phi, for instance, L2 caches are shared 
%between two cores.
\end{itemize}
~\\
\begin{minipage}{0.63\textwidth}
\includegraphics[width=.95 \textwidth]{figures/KNL_two_tiles.png}
\end{minipage}
\hfill
\begin{minipage}{0.30\textwidth}
A portion of the {\tt lstopo} PNG output for an Intel Knights Landing node, showing two tiles.\\
~\\
Cores within a tile share the L2 cache.
\end{minipage}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Additional Process Placement Considerations and Details}

\begin{itemize}
\item Placing consecutive MPI ranks on cores that share the same L2 cache may benefit performance if 
the two ranks communicate frequently with each other, because the latency between cores sharing an L2 
cache may be roughly half that of two cores not sharing one.
\item There may be benefit, however, in placing consecutive ranks on cores that do not share an L2 cache, 
because (if there are fewer MPI ranks than cores) this increases the total L2 cache capacity and bandwidth 
available to the application.
\item There is a trade-off to be considered between placing processes close together (in terms of shared resources) to 
optimize for efficient communication and synchronization vs.\ farther apart to maximize available resources (memory 
channels, caches, I/O channels, etc.)
\item The best strategy will depend on the application and the software and hardware stack.
\item Different process placement strategies can affect performance at least as much as some more commonly 
explored settings, i.e.\ compiler optimization levels.
\item To make sense of CPU IDs in process placement info, use the Portable Hardware Locality (hwloc) software package's 
      {\tt lstopo} command. If not already on your system, {\tt configure} can install it via {\tt --download-hwloc}.
\end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{KNL Process Placement: SNES tutorial example ex19}

\begin{lstlisting}[basicstyle=\ttfamily \footnotesize,language=]
$ export I_MPI_DEBUG=5 # So mappings will be printed
$ export I_MPI_PIN_DOMAIN=auto:compact
$ mpirun -n 68 numactl -p 1 ./ex19 -da_refine 7 -log_view
...
[0] MPI startup(): Rank    Pid      Node name             Pin cpu
[0] MPI startup(): 0       53095    isdp001.cels.anl.gov  {0,68,136,204}
[0] MPI startup(): 1       53096    isdp001.cels.anl.gov  {1,69,137,205}
[0] MPI startup(): 2       53097    isdp001.cels.anl.gov  {2,70,138,206}
[0] MPI startup(): 3       53098    isdp001.cels.anl.gov  {3,71,139,207}
...
                         Max       Max/Min        Avg      Total
Time (sec):           7.093e+00      1.00007   7.093e+00

$ export I_MPI_PIN_DOMAIN=auto:scatter
$ mpirun -n 68 numactl -p 1 ./ex19 -da_refine 7 -log_view
...
[0] MPI startup(): Rank    Pid      Node name             Pin cpu
[0] MPI startup(): 0       53335    isdp001.cels.anl.gov  {0,2,4,6}
[0] MPI startup(): 1       53336    isdp001.cels.anl.gov  {68,70,72,74}
[0] MPI startup(): 2       53337    isdp001.cels.anl.gov  {136,138,140,142}
[0] MPI startup(): 3       53338    isdp001.cels.anl.gov  {204,206,208,210}
...
                         Max       Max/Min        Avg      Total 
Time (sec):           1.327e+01      1.00005   1.327e+01
\end{lstlisting}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
