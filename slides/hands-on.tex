%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Exercise 0: Graph the topology of your system}

Use {\tt lstopo} to graph the topology of your system. \\
~\\
If {\tt lstopo} is not present on your system, install {\tt hwloc} via your 
package manager (e.g., {\tt brew install hwloc} or {\tt apt-get install hwloc}), 
or have PETSc {\tt configure} download via {\tt --download-hwloc}.\\
~\\
Can generate an ASCII summary by simply executing {\tt lstopo} (or possibly {\tt lstopo-no-graphics}, 
depending on how {\tt hwloc} has been built), or get a fancier ASCII art or PNG version by doing 
{\tt lstopo topo.txt} or {\tt lstopo.png}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Exercise 1: Measure sustainable memory bandwidth via STREAM Triad}

The STREAM Triad benchmark (see \url{https://www.cs.virginia.edu/stream/}) can provide a good idea of the possible 
speedup that can be obtained with a memory bandwidth-bound code on a given machine.\\
~\\
Execute the STREAM Triad benchmark on your machine.\\
Set {\tt PETSC\_ARCH} and then, in {\tt \$PETSC\_DIR}, do {\tt make streams}.\\
~\\
If your machine has multiple sockets (or perhaps just a complicated cache hierarchy), your results may vary depending 
on how MPI processes are assigned to cores. (See \url{https://www.mcs.anl.gov/petsc/documentation/faq.html#computers} for 
some notes on doing this with MPICH and OpenMP.) Example:

\begin{lstlisting}[basicstyle=\ttfamily \footnotesize, escapechar=\#]
$ make streams MPI_BINDING="--bind-to core --map-by socket"
\end{lstlisting}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Exercise 2: Examine scaling of SNES ex19 with default solvers}
Run SNES ex19 (nonlinear driven cavity problem) with default PETSc solvers:
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize, escapechar=\#]
$ cd $PETSC_DIR/src/snes/examples/tutorials; make ex19
$ mpirun -np $NP ./ex19 -da_refine $NREFINE -snes_monitor -snes_view -log_view
\end{lstlisting}

The problem size is controlled by specifying the number of times to refine the $4 \times 4$ DMDA.
(Total number of degrees of freedom is $4 \times (3 \times 2^{nrefine} + 1)^2$). Work with sizes such that that runs do not 
complete instantly; {\tt -da\_refine 5} might be a good starting point.\\
~\\
How does the strong scaling (varying NP for fixed problem size) of overall execution time and events such as MatMult 
compare to the STREAM triad scaling? You may also want to try static scaling (varying problem size for fixed NP).\\
~\\
Using the block-oriented format BAIJ reduces the size of the $j$ index array by a factor of the block size (4 for this
example.)
Try running with BAIJ matrices ({\tt -dm\_mat\_type baij}) to see how the corresponding reduction in the memory bandwidth requirements affect MatMult performance.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Exercises 3--5: Run SNES ex19 with a variety of solvers on CPU and GPU}

We can use the cuSPARSE/CUDA backend to run on NVIDIA GPUs:
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize, escapechar=\#]
$ mpirun -np $NP ./ex19 -da_refine $NREFINE -snes_monitor -snes_view -log_view 
-dm_mat_type aijcusparse -dm_vec_type cuda
\end{lstlisting}
or use the ViennaCL backend to run on any type of GPU ({\tt -dm\_mat\_type aijviennacl -dm\_vec\_type viennacl}).\\
~\\
Let's run SNES ex19 several ways, comparing GPU and CPU.\\
~\\
Aside: It may be helpful to use the nested logging capability of PETSc to understand where GPU$\leftrightarrow$CPU 
transfers are incurred. To do so, run with {\tt -log\_view :filename.xml:ascii\_xml}. The XML file may be viewable directly in
your web browser, or (my preference), do
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize, escapechar=\#]
$ ${PETSC_DIR}/lib/petsc/bin/petsc-performance-view filename.xml
\end{lstlisting}
to open a nicely-rendered version in your web browser.

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Exercise 3: Run SNES ex19 on CPU and GPU with a very simple preconditioner, Jacobi}

Use {\tt{-pc\_type jacobi}} with GPU and non-GPU cases, trying some different values for -da\_refine.\\
Example using cuSPARSE:
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize, escapechar=\#]
$ mpirun -np $NP ./ex19 -da_refine $NREFINE -snes_monitor -snes_view -log_view 
-dm_mat_type aijcusparse -dm_vec_type cuda -pc_type jacobi
\end{lstlisting}
The GPU case is probably faster for most cases. What does -log\_view tell us about why?\\
~\\
Try both the cuSPARSE and ViennaCL back-ends, if you have them both available.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Exercise 4: Run SNES ex19 with ILU on CPU and GPU}

Let's run with a more complicated preconditioner: Incomplete LU factorization (ILU)\\
~\\
Run with {\tt -pc\_type ilu}, which will do ILU in the single-process case, or with {\tt -pc\_type bjacobi} to use 
block-Jacobi with ILU applied on each block, or subdomain, in the parallel case.\\
~\\
Compared to the Jacobi case, the CPU likely becomes more competitive with the GPU.\\
Can you find a -da\_refine value where there is a change in which is faster?\\
Or identify such a point in a strong-scaling scenario? \\
~\\
Bonus: Can also try using ViennaCL for {\tt -pc\_type chowiluviennacl} on the GPU, which is likely faster overall but probably requires more Krylov iterations.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]{Exercise 5: Run SNES ex19 with multigrid on CPU and GPU}

Now use what we really {\bf ought} to be using: {-pc\_type mg}. Example (using Jacobi smoothers):
\begin{lstlisting}[basicstyle=\ttfamily \footnotesize, escapechar=\#]
$ mpirun -np $NP ./ex19 -da_refine $NREFINE -snes_monitor -snes_view -log_view
-dm_mat_type aijcusparse -dm_vec_type cuda -pc_type mg -mg_levels_pc_type jacobi
-pc_mg_levels $NLEVELS
\end{lstlisting}
{\tt NLEVELS} is the number of multigrid levels to employ; you may need to manually set this to something less than {\tt \$NREFINE}.\\
~\\
The CPU may become more competitive because the small, coarse grid problems are not well suited to GPUs.\\
~\\
Is the CPU or GPU consistently better on your system, or is there a crossover point (in strong or static scaling 
scenarios) in the behavior?\\
Can you improve GPU performance by limiting the number of levels in the multigrid hierarchy?\\
Does the CPU or GPU benefit more from using a different smoother, say, SOR? ({\tt -mg\_levels\_pc\_type sor})\\
~\\
We can play several other games. By not always updating the Jacobian, we get worse convergence and will spend more time in 
events such as MatMult, but the trade-off may be advantageous if MatMult is fast. To try this, run with 
{\tt -snes\_lag\_jacobian <lag>}.

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
